import * as todoApis from '../apis/todo';
import * as todoConstants from '../constants/todo';

export const fetchTodo = () => ({
  type: todoConstants.FETCH_TODO,
});

export const fetchTodoSuccess = (data) => ({
  type: todoConstants.FETCH_TODO_SUCCESS,
  payload: {
    data,
  },
});


export const fetchTodoFailed = (error) => ({
  type: todoConstants.FETCH_TODO_FAILED,
  payload: {
    error,
  },
});

export const filterTodo = (keyword) => ({
  type: todoConstants.FILTER_TODO,
  payload: {
    keyword,
  },
});

export const filterTodoSuccess = (dataSearch) => ({
  type: todoConstants.FILTER_TODO_SUCCESS,
  payload: {
    dataSearch,
  },
});


export const fetchTodoRequest = () => (dispatch) => {
  dispatch(fetchTodo());
  todoApis.fetchTodo()
    .then((response) => {
      const { data } = response;
      dispatch(fetchTodoSuccess(data));
    })
    .catch((error) => {
      dispatch(fetchTodoFailed(error));
    });
};


export const addTodo = (title) => ({
  type: todoConstants.ADD_TODO,
  payload: {
    title,
  }
});

export const addTodoSuccess = (data) => ({
  type: todoConstants.ADD_TODO_SUCCESS,
  payload: {
    data,
  },
});


export const addTodoFailed = (error) => ({
  type: todoConstants.ADD_TODO_FAILED,
  payload: {
    error,
  },
});


export const deleteTodo = (id) => ({
  type: todoConstants.DELETE_TODO,
  payload: {
    id,
  }
});

export const deleteTodoSuccess = (id) => ({
  type: todoConstants.DELETE_TODO_SUCCESS,
  payload: {
    id,
  },
});


export const deleteTodoFailed = (error) => ({
  type: todoConstants.DELETE_TODO_FAILED,
  payload: {
    error,
  },
});


export const updateTodo = (todo) => ({
  type: todoConstants.UPDATE_TODO,
  payload: {
    todo,
  }
});

export const updateTodoSuccess = (data) => ({
  type: todoConstants.UPDATE_TODO_SUCCESS,
  payload: {
    data,
  },
});

export const updateTodoFailed = (error) => ({
  type: todoConstants.UPDATE_TODO_FAILED,
  payload: {
    error,
  },
});

export const searchTodo = (title) => ({
  type: todoConstants.SEARCH_TODO,
  payload: {
    title,
  }
});

export const searchTodoSuccess = (data) => ({
  type: todoConstants.SEARCH_TODO_SUCCESS,
  payload: {
    data,
  },
});

export const searchTodoFailed = (error) => ({
  type: todoConstants.SEARCH_TODO_FAILED,
  payload: {
    error,
  },
});
