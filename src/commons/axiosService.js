import axios from 'axios';

class AxiosService {
  constructor() {
    const instance = axios.create();
    instance.interceptors.response.use(this.handleSuccess, this.handleError);
    this.instance = instance;
  }

  handleSuccess(response) {
    return response;
  }

  handleError(error) {
    if(!error.response) {
      return Promise.resolve({
        status: 401,
        statusText: 'Network Error',
      });
    }
    return Promise.resolve(error);
  }

  get(url) {
    return this.instance.get(url);
  }

  post(url, body) {
    return this.instance.post(url, body);
  }

  delete(url, id) {
    return this.instance.delete(`${url}/${id}`);
  }

  put(url, data) {
    return this.instance.put(`${url}/${data.id}`, data);
  }
}
export default new AxiosService();
