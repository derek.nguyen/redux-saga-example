import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { compose } from 'redux';
import { connect } from 'react-redux';
import loadingImage from '../../assets/images/loading.gif';
import './index.scss';

class Loading extends Component {
  render() {
    const { show } = this.props;
    return (
      (show && (
      <div className="loading">
        <img src={loadingImage} alt="loading" className="loading__img" />
      </div>
      ))
    );
  }
}

const mapStateToProps = (state) => ({
  show: state.ui.showLoading,
});

const withConnect = connect(mapStateToProps, null);

Loading.propTypes = {
  show: PropTypes.bool.isRequired,
};

export default compose(withConnect)(Loading);
