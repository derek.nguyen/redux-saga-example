import React from 'react';
import PropTypes from 'prop-types';

class AddForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      title: '',
    };
  }

  onChange = (e) => {
    const value = e.target.value;
    this.setState({
      title: value,
    });
    const { handleSubmit, isAdd } = this.props;
    if (isAdd) {
      return;
    }
    handleSubmit(value);
  }

  handleSubmit = (e) => {
    e.preventDefault();
    const { handleSubmit, isAdd } = this.props;
    const { title } = this.state;

    if (!isAdd) {
      return;
    }

    handleSubmit(title);
    this.setState({
      title: '',
    });
  }

  render() {
    const { title } = this.state;
    const { isAdd } = this.props;
    return (
      <form className="add-form" onSubmit={this.handleSubmit}>
        <div className="form-group mb-0 mr-2 add-form__wrap-input">
          <input
            type="text"
            className="form-control"
            id="title"
            placeholder={isAdd ? 'Add' : 'Search'}
            value={title}
            onChange={this.onChange}
            autoComplete="off"
          />
        </div>
      </form>
    );
  }
}

AddForm.propTypes = {
  handleSubmit: PropTypes.func.isRequired,
  isAdd: PropTypes.bool.isRequired,
};

export default AddForm;
