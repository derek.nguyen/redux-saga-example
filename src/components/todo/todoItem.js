import React, { Component } from 'react';
import PropTypes from 'prop-types';
import './index.scss';

class TodoItem extends Component {
  deleteTodo = (id) => {
    const { deleteTodo } = this.props;
    deleteTodo(id);
  }

  updateTodo = (todo) => {
    const { updateTodo } = this.props;
    updateTodo(todo);
  }

  updateTodoStatus = (todo) => {
    const todoUpdate = {
      ...todo,
      status: !todo.status,
    };
    this.updateTodo(todoUpdate);
  }

  render() {
    const { data } = this.props;
    return (
      <div className={`todo-item ${data.status ? 'todo-item--done' : ''}`}>
        <div className="todo-item__title" onClick={() => this.updateTodoStatus(data)}>{data.title}</div>
        <div className="todo-item__action">
          <button onClick={() => this.deleteTodo(data.id)} type="button">x</button>
        </div>
      </div>
    );
  }
}

TodoItem.propTypes = {
  data: PropTypes.object.isRequired,
  deleteTodo: PropTypes.func.isRequired,
  updateTodo: PropTypes.func.isRequired,
};

export default TodoItem;
