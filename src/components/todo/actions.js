import React, { Component } from 'react';
import PropTypes from 'prop-types';

class Actions extends Component {
  changeAddSearch = (status) => {
    const { changeAddSearch } = this.props;
    changeAddSearch(status);
  }

  render() {
    const { items } = this.props;
    return (
      <div className="todo-actions text-center">
        <button
          type="button"
          className="btn btn-primary"
          onClick={() => this.changeAddSearch(true)}
        >
            Add
        </button>
        <button
          type="button"
          className="btn btn-secondary ml-2"
          onClick={() => this.changeAddSearch(false)}
        >
            Search
        </button>
        <span className="ml-2">
          {`${items} items`}
        </span>
      </div>
    );
  }
}

Actions.propTypes = {
  changeAddSearch: PropTypes.func.isRequired,
  items: PropTypes.number.isRequired
};

export default Actions;
