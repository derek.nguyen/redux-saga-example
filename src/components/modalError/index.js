import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {bindActionCreators, compose} from 'redux';
import { connect } from 'react-redux';
import { Modal } from 'react-bootstrap';
import * as errorActions from "../../actions/error";

class ModalError extends Component {

  closeModalError = () => {
    const { errorActionCreator } = this.props;
    const { hideError } = errorActionCreator;
    hideError();
  }

  render() {
    const { error } = this.props;
    return (
      <Modal show={error.show} onHide={this.closeModalError} centered>
        <Modal.Header closeButton>
          <Modal.Title>Error</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <div className="form-group">
            <span className="text-danger">{error.errorText}</span>
          </div>
        </Modal.Body>
      </Modal>
    );
  }
}

const mapStateToProps = (state) => ({
  error: state.error,
});

const mapDispatchToProps = (dispatch) => ({
  errorActionCreator: bindActionCreators(errorActions, dispatch),
});

const withConnect = connect(mapStateToProps, mapDispatchToProps);

ModalError.propTypes = {
  error: PropTypes.object.isRequired,
  errorActionCreator: PropTypes.object,
};

export default compose(withConnect)(ModalError);
