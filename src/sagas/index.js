import {
  fork, take, delay, call, put, takeLatest, takeEvery
} from 'redux-saga/effects';
import * as todoTypes from '../constants/todo';
import {
  addTodo, fetchTodo, deleteTodo, updateTodo, searchTodo,
} from '../apis/todo';
import { STATUS_CODE } from '../constants';
import {
  addTodoFailed,
  addTodoSuccess,
  fetchTodoFailed,
  fetchTodoSuccess,
  deleteTodoSuccess,
  deleteTodoFailed,
  updateTodoSuccess,
  updateTodoFailed,
  searchTodoSuccess,
  searchTodoFailed,
} from '../actions/todo';
import { showLoading, hideLoading } from '../actions/ui';
import { showError, hideError } from '../actions/error';

function* watchFetchTodoAction() {
  // take is blocking
  while (true) {
    yield take(todoTypes.FETCH_TODO);
    yield put(showLoading());
    // call is blocking
    const todos = yield call(fetchTodo);
    const { status, statusText, data } = todos;

    if (status === STATUS_CODE.SUCCESS) {
      // dispatch action fetchTodoSuccess
      yield put(fetchTodoSuccess(data));
    } else {
      // dispatch action fetchTodoFailed
      yield put(showError(statusText));
      //yield put(fetchTodoFailed(statusText));
    }

    yield delay(500);
    yield put(hideLoading());
  }
}

function* addTodoSaga({ payload }) {
  yield put(showLoading());
  const { title } = payload;
  const response = yield call(addTodo, { title, status: false });
  const { data, status } = response;
  if (status === STATUS_CODE.CREATED) {
    yield put(addTodoSuccess(data));
  } else {
    yield put(addTodoFailed(data));
  }
  yield delay(500);
  yield put(hideLoading());
}

function* deleteTodoSaga({ payload }) {
  yield put(showLoading());
  const { id } = payload;
  const response = yield call(deleteTodo, id);
  const { data, status } = response;

  if (status === STATUS_CODE.SUCCESS) {
    yield put(deleteTodoSuccess(id));
  } else {
    yield put(deleteTodoFailed(data));
  }
  yield delay(500);
  yield put(hideLoading());
}

function* updateTodoSaga({ payload }) {
  yield put(showLoading());
  const { todo } = payload;
  const response = yield call(updateTodo, todo);
  const { data, status } = response;

  if (status === STATUS_CODE.SUCCESS) {
    yield put(updateTodoSuccess(data));
  } else {
    yield put(updateTodoFailed(data));
  }
  yield delay(500);
  yield put(hideLoading());
}

function* searchTodoSaga({ payload }) {
  yield put(showLoading());
  const { title } = payload;
  const response = yield call(searchTodo, title);
  const { data, status } = response;

  if (status === STATUS_CODE.SUCCESS) {
    yield put(searchTodoSuccess(data));
  } else {
    yield put(searchTodoFailed(data));
  }
  yield delay(500);
  yield put(hideLoading());
}

function* rootSaga() {
  // fork is non-blocking
  yield fork(watchFetchTodoAction);
  yield takeEvery(todoTypes.ADD_TODO, addTodoSaga);
  yield takeEvery(todoTypes.DELETE_TODO, deleteTodoSaga);
  yield takeEvery(todoTypes.UPDATE_TODO, updateTodoSaga);
  yield takeLatest(todoTypes.SEARCH_TODO, searchTodoSaga);
}

export default rootSaga;
