import React from 'react';
import './assets/scss/style.scss';
import { Provider } from 'react-redux';
import TodoMain from './containers/todoMain';
import configureStore from './redux/configuareStore';
import 'react-toastify/dist/ReactToastify.css';
import Loading from './components/loading';
import ModalError from "./components/modalError";

const store = configureStore();
function App() {
  return (
    <Provider store={store}>
      <div className="App">
        <div className="container">
          <TodoMain />
        </div>
        <Loading show />
        <ModalError />
      </div>
    </Provider>
  );
}

export default App;
