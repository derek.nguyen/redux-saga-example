import { combineReducers } from 'redux';
import todoReducer from './todo';
import uiReducer from './ui';
import errorReducer from './error';

const rootReducer = combineReducers({
  todo: todoReducer,
  ui: uiReducer,
  error: errorReducer,
});
export default rootReducer;
