import * as todoActionType from '../constants/todo';

const initialState = {
  todoList: [],
  error: '',
};
const todoReducer = (state = initialState, action) => {
  switch (action.type) {
    case todoActionType.FETCH_TODO:
      return {
        ...state,
        todoList: [],
      };
    case todoActionType.FETCH_TODO_SUCCESS:
      return {
        ...state,
        todoList: action.payload.data,
      };
    case todoActionType.FETCH_TODO_FAILED:
      return {
        ...state,
      };
    case todoActionType.FILTER_TODO_SUCCESS:
      return {
        ...state,
        todoList: action.payload.dataSearch,
      };

    case todoActionType.ADD_TODO:
      return {
        ...state,
      };
    case todoActionType.ADD_TODO_SUCCESS:
      return {
        ...state,
        todoList: [...state.todoList, ...[action.payload.data]],
      };
    case todoActionType.ADD_TODO_FAILED:
      return {
        ...state,
        error: action.payload.error,
      };


    case todoActionType.DELETE_TODO:
      return {
        ...state,
      };
    case todoActionType.DELETE_TODO_SUCCESS:
      return {
        ...state,
        todoList: state.todoList.filter((item) => item.id !== action.payload.id),
      };
    case todoActionType.DELETE_TODO_FAILED:
      return {
        ...state,
        error: action.payload.error,
      };

    case todoActionType.UPDATE_TODO:
      return {
        ...state,
      };
    case todoActionType.UPDATE_TODO_SUCCESS:
      const todoLists = [...state.todoList];

      const todoIndex = todoLists.findIndex((item) => item.id === action.payload.data.id);
      if (todoIndex !== -1) {
        todoLists[todoIndex] = action.payload.data;
      }

      return {
        ...state,
        todoList: todoLists,
      };
    case todoActionType.UPDATE_TODO_FAILED:
      return {
        ...state,
        error: action.payload.error,
      };

    case todoActionType.SEARCH_TODO:
      return {
        ...state,
      };
    case todoActionType.SEARCH_TODO_SUCCESS:
      return {
        ...state,
        todoList: action.payload.data,
      };
    case todoActionType.SEARCH_TODO_FAILED:
      return {
        ...state,
        error: action.payload.error,
      };

    default:
      return state;
  }
};
export default todoReducer;
