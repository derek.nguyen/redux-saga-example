import axiosService from '../commons/axiosService';
import { API_URL } from '../constants/config';

const APP_URL = `${API_URL}/todos`;

export const fetchTodo = () => axiosService.get(APP_URL);

export const addTodo = (body) => axiosService.post(APP_URL, body);

export const deleteTodo = (id) => axiosService.delete(APP_URL, id);

export const updateTodo = (data) => axiosService.put(APP_URL, data);

export const searchTodo = (title) => axiosService.get(`${APP_URL}?title_like=${title}`);
