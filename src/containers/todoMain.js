import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import PropTypes from 'prop-types';
import * as todoActions from '../actions/todo';
import AddForm from '../components/todo/addForm';
import TodoItem from '../components/todo/todoItem';
import Actions from '../components/todo/actions';

class TodoMain extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isAdd: true,
    };
  }

  componentDidMount() {
    const { todoActionCreator } = this.props;
    const { fetchTodo } = todoActionCreator;
    fetchTodo();
  }

  handleSubmit = (title) => {
    const { isAdd } = this.state;
    const { todoActionCreator } = this.props;

    const { addTodo, searchTodo } = todoActionCreator;
    if (isAdd) {
      addTodo(title);
    } else {
      searchTodo(title);
    }
  }

  deleteTodo = (id) => {
    const { todoActionCreator } = this.props;
    const { deleteTodo } = todoActionCreator;
    deleteTodo(id);
  }

  updateTodo = (todo) => {
    const { todoActionCreator } = this.props;
    const { updateTodo } = todoActionCreator;
    updateTodo(todo);
  }

  changeAddSearch = (status) => {
    this.setState({
      isAdd: status,
    });
  }

  render() {
    const { todoList } = this.props;
    const { isAdd } = this.state;

    return (
      <div className="row justify-content-center vh-100">
        <div className="col-md-8 col-lg-6 align-self-center">
          <div className="card">
            <div className="card-header text-uppercase text-center">
              <strong>TODOS</strong>
            </div>
            <div className="card-body">
              <AddForm handleSubmit={this.handleSubmit} isAdd={isAdd} />
              <div className="list mt-3">
                {todoList && todoList.map((item) => (
                  <TodoItem
                    key={item.id}
                    data={item}
                    deleteTodo={this.deleteTodo}
                    updateTodo={this.updateTodo}
                  />
                ))}
              </div>
            </div>
            <div className="card-footer">
              <Actions
                changeAddSearch={this.changeAddSearch}
                items={todoList ? todoList.length : 0}
              />
            </div>
          </div>
        </div>
      </div>
    );
  }
}

TodoMain.propTypes = {
  todoActionCreator: PropTypes.object,
  todoList: PropTypes.array,
};

TodoMain.defaultProps = {
  todoActionCreator: undefined,
  todoList: [],
};

const mapStateToProps = (state) => ({
  todoList: state.todo.todoList,
});
const mapDispatchToProps = (dispatch) => ({
  todoActionCreator: bindActionCreators(todoActions, dispatch),
});

export default (connect(mapStateToProps, mapDispatchToProps))(TodoMain);
