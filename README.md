1. install json server:
- npm install -g json-server
- creat db.json file: 
{
  "todos": [
    {
      "title": "json-server",
      "status": true,
      "id": 1
    },
    {
      "title": "reactjs",
      "status": true,
      "id": 2
    }
  ]
}
- start json server: json-server --watch db.json --port 3004
- detail about json server: https://github.com/typicode/json-server
2. npm start